# PavlovTTTSpyFall

Before the game starts all terrorists will be given the location in the map where the REAL bomb was planted. Only 2 spies will not know where the location where the bomb was planted. People have to figure out who the traitors are by asking them questions about the location.  If the spy finds out what the location is he can then go to the location and deactivate the bomb by pressing the deactivation trigger button.

If the traitors deactivate the wrong bomb they will lose and the game will end. There are 7 fake bombs (this number is subject to change) and 1 real one. If the timer runs out the REAL bomb will explode ending the game and making the terrorist win. 

Optional: Each bomb locaiton has a timer counting down that is reset whenever a player is stepping near the bomb. The terrorist have to constantly patrol all the spots. If they don’t they lose.

